package com.rep.organiza.organizarep.components.weekDays;

import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by gabriel on 11/05/19.
 */

public class DayView {

    DayOfWeek dayOfWeek;
    TextView dayDate;
    CircleImageView icon;

    public DayView(DayOfWeek dayOfWeek, TextView dayDate, CircleImageView icon) {
        this.dayDate = dayDate;
        this.icon = icon;
        this.dayOfWeek = dayOfWeek;
    }

    public TextView getDayDate() {
        return dayDate;
    }

    public CircleImageView getIcon() {
        return icon;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }
}
