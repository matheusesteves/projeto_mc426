package com.rep.organiza.organizarep.alert.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.rep.organiza.organizarep.Constants;
import com.rep.organiza.organizarep.alert.model.BaseAlert;
import com.rep.organiza.organizarep.alert.model.BroadcastAlert;
import com.rep.organiza.organizarep.alert.model.DelayedTaskAlert;
import com.rep.organiza.organizarep.alert.model.TypeAlert;
import com.rep.organiza.organizarep.alert.view.ListAlertsFragment;
import com.rep.organiza.organizarep.api.ResponseInterceptor;
import com.rep.organiza.organizarep.api.Services;
import com.rep.organiza.organizarep.base.BasePresenter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListAlertsPresenter extends BasePresenter {
    private static boolean isDataLoaded = false;
    private ListAlertsFragment fragment;
    private List<BaseAlert> listAlerts;

    public ListAlertsPresenter(ListAlertsFragment fragment){
        this.fragment = fragment;
        this.listAlerts = new ArrayList<>();
    }
    public void loadAlerts(){
        fragment.showLoading();
        ResponseInterceptor.getInstance().intercept(this.fragment.getContext(), this, getAlertsCallback(),
                Services.GET_ALERTS, new JsonObject());
    }

    private class ListAlertsGsonAdapter extends TypeAdapter<BaseAlert> {

        @Override
        public BaseAlert read(JsonReader in) throws IOException{
            BaseAlert item = null;
            Gson gson = new Gson();
            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = (JsonObject)jsonParser.parse(in);
            JsonElement jsonElementType = jsonObject.get("type");
            String type = (jsonElementType != null) ? jsonElementType.getAsString() : "";
            if (type.equals(TypeAlert.late_task.getKey())) {
                item = gson.fromJson(jsonObject, DelayedTaskAlert.class);
            } else {
                if (type.equals(TypeAlert.broadcast_alert.getKey())){
                    item = gson.fromJson(jsonObject, BroadcastAlert.class);
                }
            }
            return item;
        }

        @Override
        public void write(JsonWriter out, BaseAlert item){}
    }

    @NonNull
    private Callback<JsonArray> getAlertsCallback() {
        return new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                if (response.body() != null && response.isSuccessful()) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.registerTypeAdapter(BaseAlert.class, new ListAlertsGsonAdapter());
                    Gson gson = gsonBuilder.create();
                    Type listType = new TypeToken<List<BaseAlert>>() {}.getType();
                    listAlerts = gson.fromJson(response.body(), listType);
                    showSuccessfulAlerts(listAlerts);
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable throwable) {
                Log.e(Constants.REPOSITORY_TAG, "Falha ao recuperar alertas "+throwable.toString());
                fragment.failure();
                return;
            }
        };
    }

    public void showSuccessfulAlerts(List<BaseAlert> alerts){
        fragment.showAlerts(alerts);
        fragment.hideLoading();
    }
}
