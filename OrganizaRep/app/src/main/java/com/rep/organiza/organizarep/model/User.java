package com.rep.organiza.organizarep.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {

    @SerializedName("name")
    private String userName;

    @SerializedName("facebook_id")
    private String id;

    @SerializedName("photo_url")
    private String userImageUrl;

    public User(String userName, String userImageUrl, String id) {
        this.userImageUrl = userImageUrl;
        this.userName = userName;
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public void setUserImageUrl(String userImageUrl) {
        this.userImageUrl = userImageUrl;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    @Override
    public boolean equals(Object other){
        if (other == null){
            return false;
        }
        if (other instanceof User){
            User otherUser = (User)other;
            return (this.id == otherUser.id || this.id.equals(otherUser.id));
        }
        return false;
    }
}
