package com.rep.organiza.organizarep.task.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.JsonObject;
import com.rep.organiza.organizarep.api.ResponseInterceptor;
import com.rep.organiza.organizarep.api.Services;
import com.rep.organiza.organizarep.base.BasePresenter;
import com.rep.organiza.organizarep.model.User;
import com.rep.organiza.organizarep.task.view.AlertTaskFragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlertTaskPresenter extends BasePresenter {
    private AlertTaskFragment fragment;

    public AlertTaskPresenter(AlertTaskFragment fragment){
        this.fragment = fragment;
    }

    public void sendAlert(int id){
        JsonObject taskNotification = new JsonObject();
        JsonObject taskId = new JsonObject();

        taskId.addProperty("task_id", id);
        taskNotification.add("taskNotification", taskId);

        Log.d("sendAlert", taskNotification.toString());

        ResponseInterceptor.getInstance().intercept(this.fragment.getContext(), this, sendAlertCallback(), Services.ALERT_TASK, taskNotification);
    }

    @NonNull
    private Callback sendAlertCallback() {
        return new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.body() != null && response.isSuccessful()) {
                    fragment.successfulAlert();
                }
            }

            @Override
            public void onFailure(Call call, Throwable throwable) {
                fragment.failure();
            }
        };
    }
}
