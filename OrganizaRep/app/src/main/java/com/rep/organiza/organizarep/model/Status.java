package com.rep.organiza.organizarep.model;

import java.io.Serializable;

public enum Status implements Serializable {
    done("DO"),
    late("LA"),
    exchanged("EX"),
    toDo("TO"),
    notTaskDay("ND");

    private String key;

    Status(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
