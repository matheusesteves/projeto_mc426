package com.rep.organiza.organizarep.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.rep.organiza.organizarep.Constants;
import com.rep.organiza.organizarep.R;
import com.rep.organiza.organizarep.Util.FragmentManager;
import com.rep.organiza.organizarep.alert.view.AlertActivity;
import com.rep.organiza.organizarep.helper.NetworkHelper;
import com.rep.organiza.organizarep.home.view.HomeActivity;
import com.rep.organiza.organizarep.task.view.CreateTaskFragment;
import com.rep.organiza.organizarep.task.view.ListTasksFragment;
import com.rep.organiza.organizarep.task.view.TaskActivity;

import butterknife.Bind;
import butterknife.ButterKnife;


public class BaseActivity extends AppCompatActivity {

    protected ActionBar mActionBar;
    private static int mForegroundActivities = 0;
    private boolean inBackground = false;
    BottomNavigationView navbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void setNavbarOnClick(int selectedItem) {
        navbar = findViewById(R.id.navbar);
        navbar.setSelectedItemId(selectedItem);
        navbar.setOnNavigationItemSelectedListener(
                item -> {
                    Intent intent;
                    switch (item.getItemId()) {
                        case R.id.item_alerts:
                            if (!(this instanceof AlertActivity)) {
                                intent = new Intent(this, AlertActivity.class);
                                startActivity(intent);
                            }
                            return true;
                        case R.id.item_tasks:
                            if (!(this instanceof TaskActivity)) {
                                intent = new Intent(this, TaskActivity.class);
                                startActivity(intent);
                            }
                            return true;
                        case R.id.item_goals:
                            //TODO onclick da tela de Metas
                            return true;
                        case R.id.item_finances:
                            //TODO onclick da tela de Finanças
                            return true;
                    }
                    return false;
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mForegroundActivities++;

        if (mForegroundActivities == 1) {
            Log.i(Constants.ACTIVITY_CONTROL, "in foreground");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        inBackground = false;

        if (!NetworkHelper.isOnline(this)) {
            Snackbar.make(this.getWindow().getDecorView().getRootView(), R.string.network_not_connected, Snackbar.LENGTH_INDEFINITE).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        inBackground = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        inBackground = true;
        mForegroundActivities--;

        if (mForegroundActivities == 0) {
            Log.i(Constants.ACTIVITY_CONTROL, "in background");
        }
    }

    public static boolean isAppInForeground() {
        return mForegroundActivities == 1;
    }

    public boolean isAlive() {
        return !inBackground && !isFinishing();
    }
}
