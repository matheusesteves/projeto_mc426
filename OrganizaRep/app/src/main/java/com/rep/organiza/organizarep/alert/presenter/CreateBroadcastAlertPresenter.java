package com.rep.organiza.organizarep.alert.presenter;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.JsonObject;
import com.rep.organiza.organizarep.alert.view.CreateBroadcastAlertFragment;
import com.rep.organiza.organizarep.api.ResponseInterceptor;
import com.rep.organiza.organizarep.api.Services;
import com.rep.organiza.organizarep.base.BasePresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateBroadcastAlertPresenter extends BasePresenter {
    private CreateBroadcastAlertFragment fragment;

    public CreateBroadcastAlertPresenter(CreateBroadcastAlertFragment fragment){
        this.fragment = fragment;
    }

    public void sendBroadcastAlert(String title, String description){
        JsonObject broadcastAlert = new JsonObject();
        JsonObject broadcastAlertData = new JsonObject();
        broadcastAlertData.addProperty("title", title);
        broadcastAlertData.addProperty("description", description);
        broadcastAlert.add("broadcastNotification", broadcastAlertData);

        Log.d("Send broadcast alert : ", broadcastAlert.toString());

        ResponseInterceptor.getInstance().intercept(
                this.fragment.getContext(),
                this, sendAlertCallback(),
                Services.SEND_BROADCAST_ALERT, broadcastAlert
        );
    }

    @NonNull
    private Callback sendAlertCallback() {
        return new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.body() != null && response.isSuccessful()) {
                    fragment.successfulAlert();
                }
            }

            @Override
            public void onFailure(Call call, Throwable throwable) {
                fragment.failure();
            }
        };
    }
}
