package com.rep.organiza.organizarep.task.view.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rep.organiza.organizarep.Constants;
import com.rep.organiza.organizarep.R;
import com.rep.organiza.organizarep.Util.FragmentManager;
import com.rep.organiza.organizarep.base.OrganizaRepApp;
import com.rep.organiza.organizarep.components.weekDays.DayView;
import com.rep.organiza.organizarep.components.weekDays.WeekDaysView;
import com.rep.organiza.organizarep.mock.UserAuthenticator;
import com.rep.organiza.organizarep.model.Task;
import com.rep.organiza.organizarep.model.Status;
import com.rep.organiza.organizarep.task.model.WeekDay;
import com.rep.organiza.organizarep.task.view.AlertTaskFragment;
import com.rep.organiza.organizarep.task.view.ChangeTaskFragment;
import com.rep.organiza.organizarep.task.view.ListTasksFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskViewHolder> {

    private List<Task> listTask;
    private Context mContext;
    private ListTasksFragment mFragment;

    public TaskAdapter(List<Task> list, Context context, ListTasksFragment fragment) {
        this.listTask = list;
        this.mContext = context;
        this.mFragment = fragment;
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task, parent, false);
        return new TaskViewHolder(v);
    }

    private void customizeAuthenticatedUserTask(TaskViewHolder holder, Task task){
        holder.ivAlert.setVisibility(View.GONE);
        holder.ivExchange.setVisibility(View.GONE);
        setImage(holder.cvUserImage, task.getUser().getUserImageUrl());
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        Task task = listTask.get(position);

        String taskTitle = task.getTitle();
        String taskDescription = task.getDescription();
        String userName = task.getUser().getUserName();
        String userImgUrl = task.getUser().getUserImageUrl();
        List<WeekDay> weekDays = task.getWeekDays();

        holder.tvTaskTitle.setText(taskTitle);
        holder.tvTaskDescription.setText(taskDescription);
        holder.tvUserName.setText(userName);
        holder.setTaskId(task.getId());

        feedWeekDays(weekDays, holder.wvFirstWeek);
        feedWeekDays(weekDays, holder.wvSecondWeek);

        holder.ivAlert.setVisibility(View.VISIBLE);
        holder.ivExchange.setVisibility(View.VISIBLE);

        if (task.getUser().equals(UserAuthenticator.getAuthenticatedUser())){
            customizeAuthenticatedUserTask(holder,task);
        } else {
            holder.setExchangeOnclick(task);
            setImage(holder.cvUserImage, userImgUrl);
        }
    }

    private void feedWeekDays(List<WeekDay> weekdays, WeekDaysView weekdaysView) {

        List<DayView> weekdaysList = weekdaysView.getWeekdays();
        WeekDay weekdayModel;
        DayView dayView;

        for (int i=0; i<7; i++){
            weekdayModel = weekdays.get(i);
            dayView = weekdaysList.get(i);

            weekdaysView.setDayStatus(dayView.getDayOfWeek(), mContext, weekdayModel.getStatus());
            weekdaysView.setWeekdayDate(dayView.getDayOfWeek(), weekdayModel.getDate());
        }
    }

    private void setImage(ImageView img, String imgUrl) {
        Picasso.with(mContext)
                .load(imgUrl)
                .placeholder(R.color.colorNotFound)
                .into(img);
    }

    @Override
    public int getItemCount() {
        return listTask != null ? listTask.size() : 0;
    }

    public class TaskViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.tv_task_title)
        TextView tvTaskTitle;

        @Bind(R.id.tv_task_description)
        TextView tvTaskDescription;

        @Bind(R.id.tv_user_name)
        TextView tvUserName;

        @Bind(R.id.cv_user_image)
        CircleImageView cvUserImage;

        @Bind(R.id.iv_alert)
        ImageView ivAlert;

        @Bind(R.id.iv_exchange)
        ImageView ivExchange;

        @Bind(R.id.wv_first_week)
        WeekDaysView wvFirstWeek;

        @Bind(R.id.wv_second_week)
        WeekDaysView wvSecondWeek;

        int taskId;

        public TaskViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            setAlertOnclick();
        }

        public void setTaskId(int taskId){
            this.taskId = taskId;
        }

        public void setAlertOnclick(){
            ivAlert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String alertTaskFragmentIdentification = "frag_2";

                    Bundle bundle = new Bundle();
                    bundle.putInt("taskId", taskId);

                    AlertTaskFragment alertTaskFragment = new AlertTaskFragment();
                    alertTaskFragment.setArguments(bundle);

                    FragmentManager.addFragment(R.id.container_task,
                            alertTaskFragment, alertTaskFragmentIdentification, false,
                            mFragment.getActivity().getSupportFragmentManager());
                }
            });
        }

        public void setExchangeOnclick(Task taskOtherUser){
            ivExchange.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String changeTaskFragmentIdentification = "frag_1";

                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.DATATRANSFERING_FROM_TASKITEM_TO_CHANGETASK, taskOtherUser);

                    ChangeTaskFragment changeTaskFragment = new ChangeTaskFragment();
                    changeTaskFragment.setArguments(bundle);

                    FragmentManager.addFragment(R.id.container_task,
                            changeTaskFragment, changeTaskFragmentIdentification, false,
                            mFragment.getActivity().getSupportFragmentManager());
                }
            });
        }
    }
}