package com.rep.organiza.organizarep.login;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.rep.organiza.organizarep.R;
import com.rep.organiza.organizarep.alert.view.AlertActivity;
import com.rep.organiza.organizarep.base.BaseActivity;
import com.rep.organiza.organizarep.base.OrganizaRepApp;
import com.rep.organiza.organizarep.model.AuthenticatedUser;
import com.rep.organiza.organizarep.task.view.TaskActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity {

    @Bind(R.id.btn_login)
    LoginButton btnLogin;

    private static final String PUBLIC_PROFILE = "public_profile";
    private CallbackManager mCallbackManager;
    private LoginPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        try {
            PackageInfo info = this.getPackageManager().getPackageInfo(
                    "com.rep.organiza.organizarep",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash", "KeyHash:" + Base64.encodeToString(md.digest(),
                        Base64.DEFAULT));
                Toast.makeText(this.getApplicationContext(), Base64.encodeToString(md.digest(),
                        Base64.DEFAULT), Toast.LENGTH_LONG).show();
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("Erro", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("Erro2", e.toString());
        }
        this.presenter = new LoginPresenter(this);
        setLoginButton();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.setLoggedUser();
    }

    private void setLoginButton() {
        btnLogin.setReadPermissions(Arrays.asList(PUBLIC_PROFILE));
        btnLogin.registerCallback(mCallbackManager, this.presenter.setLogin());
    }

    public void successfulLogin(){
        startActivity(new Intent(LoginActivity.this, AlertActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void cancelLogin() {
        Toast.makeText(this, "Para continuar é necessário fazer o login", Toast.LENGTH_SHORT).show();
    }

    public void failedLogin() {
        Toast.makeText(this, "Ocorreu um erro ao tentar fazer o login", Toast.LENGTH_SHORT).show();
    }

    public void failure() {
        Toast.makeText(this, "Ocorreu um erro ao tentar autenticar o usuário", Toast.LENGTH_SHORT).show();
    }

    public void unauthenticatedUser() {
        Toast.makeText(this, "Usuário não autenticado", Toast.LENGTH_SHORT).show();
    }
}
