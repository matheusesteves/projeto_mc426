package com.rep.organiza.organizarep.alert.model;

import com.google.gson.annotations.SerializedName;
import com.rep.organiza.organizarep.model.Task;

public class DelayedTaskAlert extends BaseAlert{
    @SerializedName("sent_task")
    private Task task;

    public DelayedTaskAlert(String date, TypeAlert type, Task task) {
        super(type, date);
        this.task = task;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}
