package com.rep.organiza.organizarep.api;

import com.google.gson.JsonObject;
import com.rep.organiza.organizarep.Constants;
import com.rep.organiza.organizarep.base.OrganizaRepApp;

import retrofit2.Call;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

@SuppressWarnings("unchecked")
public enum Services {
    AUTHENTICATE {
        @Override
        public <T> Call<T> getCall(JsonObject args) {
            String token = args.get(Constants.TOKEN).getAsString();
            return (Call<T>) OrganizaRepAPI.get().authorize(token);
        }
    },
    GET_TASKS {
        @Override
        public <T> Call<T> getCall(JsonObject args) {
            String token = OrganizaRepApp.getInstance().getToken();
            return (Call<T>) OrganizaRepAPI.get().getTasks(token);
        }
    },
    GET_USERS {
        @Override
        public <T> Call<T> getCall(JsonObject args) {
            String token = OrganizaRepApp.getInstance().getToken();
            return (Call<T>) OrganizaRepAPI.get().getUsers(token);
        }
    },
    GET_ALERTS {
        @Override
        public <T> Call<T> getCall(JsonObject args) {
            String token = OrganizaRepApp.getInstance().getToken();
            return (Call<T>) OrganizaRepAPI.get().getAlerts(token, 10, 1);
        }
    },
    SAVE_TASK {
        @Override
        public <T> Call<T> getCall(JsonObject args) {
            String token = OrganizaRepApp.getInstance().getToken();
            return (Call<T>) OrganizaRepAPI.get().saveTask(token, args);
        }
    },
    ALERT_TASK {
        @Override
        public <T> Call<T> getCall(JsonObject args) {
            String token = OrganizaRepApp.getInstance().getToken();
            return (Call<T>) OrganizaRepAPI.get().alertTask(token, args);
        }
    },
    SEND_BROADCAST_ALERT {
        @Override
        public <T> Call<T> getCall(JsonObject args) {
            String token = OrganizaRepApp.getInstance().getToken();
            return (Call<T>) OrganizaRepAPI.get().sendBroadcastAlert(token, args);
        }
    };

    public abstract <T> Call<T> getCall(JsonObject args);

}
