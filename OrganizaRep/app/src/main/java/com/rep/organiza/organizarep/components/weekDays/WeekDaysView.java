package com.rep.organiza.organizarep.components.weekDays;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.rep.organiza.organizarep.R;
import com.rep.organiza.organizarep.Util.Util;
import com.rep.organiza.organizarep.model.Status;
import com.rep.organiza.organizarep.task.view.adapters.TaskAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class WeekDaysView extends ConstraintLayout {

    @Bind(R.id.tv_sunday)
    TextView tvSundayTitle;

    @Bind(R.id.tv_sunday_date)
    TextView tvSundayDate;

    @Bind(R.id.cv_sunday_circle)
    CircleImageView cvSundayIcon;

    @Bind(R.id.tv_monday)
    TextView tvMondayTitle;

    @Bind(R.id.tv_monday_date)
    TextView tvMondayDate;

    @Bind(R.id.cv_monday_circle)
    CircleImageView cvMondayIcon;

    @Bind(R.id.tv_tuesday)
    TextView tvTuesdayTitle;

    @Bind(R.id.tv_tuesday_date)
    TextView tvTuesdayDate;

    @Bind(R.id.cv_tuesday_circle)
    CircleImageView cvTuesdayIcon;

    @Bind(R.id.tv_wednesday)
    TextView tvWednesdayTitle;

    @Bind(R.id.tv_wednesday_date)
    TextView tvWednesdayDate;

    @Bind(R.id.cv_wednesday_circle)
    CircleImageView cvWednesdayIcon;

    @Bind(R.id.tv_thursday)
    TextView tvThursdayTitle;

    @Bind(R.id.tv_thursday_date)
    TextView tvThursdayDate;

    @Bind(R.id.cv_thursday_circle)
    CircleImageView cvThursdayIcon;

    @Bind(R.id.tv_friday)
    TextView tvFridayTitle;

    @Bind(R.id.tv_friday_date)
    TextView tvFridayDate;

    @Bind(R.id.cv_friday_circle)
    CircleImageView cvFridayIcon;

    @Bind(R.id.tv_saturday)
    TextView tvSaturdayTitle;

    @Bind(R.id.tv_saturday_date)
    TextView tvSaturdayDate;

    @Bind(R.id.cv_saturday_circle)
    CircleImageView cvSaturdayIcon;

    public WeekDaysView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater)context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_week_days, this, true);

        ButterKnife.bind(this);
        setProperties(context, attrs);
    }

    private void setProperties(Context context, AttributeSet attrs) {
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.week_days);
        boolean enableDaysTitle = attributes.getBoolean(R.styleable.week_days_days_title_visibility, true);
        setDaysTitleVisibity(enableDaysTitle);
        attributes.recycle();
    }

    private void setDaysTitleVisibity(boolean enableDaysTitle) {
        int visibity = enableDaysTitle? VISIBLE : INVISIBLE;

        this.tvSundayTitle.setVisibility(visibity);
        this.tvMondayTitle.setVisibility(visibity);
        this.tvTuesdayTitle.setVisibility(visibity);
        this.tvWednesdayTitle.setVisibility(visibity);
        this.tvThursdayTitle.setVisibility(visibity);
        this.tvFridayTitle.setVisibility(visibity);
        this.tvSaturdayTitle.setVisibility(visibity);
    }


    public List<DayView> getWeekdays() {
        List list = new ArrayList<CircleImageView>();
        list.add(new DayView(DayOfWeek.Sunday, this.tvSundayDate, this.cvSundayIcon));
        list.add(new DayView(DayOfWeek.Monday, this.tvMondayDate, this.cvMondayIcon));
        list.add(new DayView(DayOfWeek.Tuesday, this.tvTuesdayDate, this.cvTuesdayIcon));
        list.add(new DayView(DayOfWeek.Wednesday, this.tvWednesdayDate, this.cvWednesdayIcon));
        list.add(new DayView(DayOfWeek.Thursday, this.tvThursdayDate, this.cvThursdayIcon));
        list.add(new DayView(DayOfWeek.Friday, this.tvFridayDate, this.cvFridayIcon));
        list.add(new DayView(DayOfWeek.Saturday, this.tvSaturdayDate, this.cvSaturdayIcon));

        return list;
    }

    private TextView getDayDate(DayOfWeek day){
        switch (day){
            case Sunday: return this.tvSundayDate;
            case Monday: return this.tvMondayDate;
            case Tuesday: return this.tvTuesdayDate;
            case Wednesday: return this.tvWednesdayDate;
            case Thursday: return this.tvThursdayDate;
            case Friday: return this.tvFridayDate;
            case Saturday: return this.tvSaturdayDate;
        }

        return null;
    }

    private CircleImageView getDayCircle(DayOfWeek day){
        switch (day){
            case Sunday: return this.cvSundayIcon;
            case Monday: return this.cvMondayIcon;
            case Tuesday: return this.cvTuesdayIcon;
            case Wednesday: return this.cvWednesdayIcon;
            case Thursday: return this.cvThursdayIcon;
            case Friday: return this.cvFridayIcon;
            case Saturday: return this.cvSaturdayIcon;
        }

        return null;
    }

    public void setWeekdayDate(DayOfWeek dayOfWeek, Date date) {
        this.getDayDate(dayOfWeek).setText(Util.dateToString(date, "dd/MM"));
    }

    public void setDayStatus(DayOfWeek dayOfWeek, Context mContext, Status status) {
        this.setDayIcon(this.getDayCircle(dayOfWeek),  mContext, status);
    }
    private int getStatusColor(Context mContext, Status status){
        switch (status){
            case done:
            case exchanged:
                return ContextCompat.getColor(mContext, R.color.colorDoneTask);

            case late:
                return ContextCompat.getColor(mContext, R.color.colorLateTask);

            case toDo:
                return ContextCompat.getColor(mContext, R.color.colorToDoTask);

            default:
                return ContextCompat.getColor(mContext, R.color.colorNotTaskDay);
        }
    }

    private void setDayIcon(CircleImageView cvIcon, Context mContext, Status status){
        if(status == Status.exchanged){
            Drawable icon = ContextCompat.getDrawable(mContext, R.drawable.ic_exchange);
            int color = ContextCompat.getColor(mContext, R.color.colorDoneTask);
            icon.setColorFilter(color, PorterDuff.Mode.SRC_IN);
            cvIcon.setImageDrawable(icon);
        }else{
            cvIcon.setColorFilter(getStatusColor(mContext, status),  PorterDuff.Mode.SRC);
        }
    }
}
