package com.rep.organiza.organizarep.alert.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.rep.organiza.organizarep.Constants;
import com.rep.organiza.organizarep.R;
import com.rep.organiza.organizarep.Util.FragmentManager;
import com.rep.organiza.organizarep.alert.presenter.CreateBroadcastAlertPresenter;
import com.rep.organiza.organizarep.base.BaseFragment;
import butterknife.Bind;
import butterknife.ButterKnife;

public class CreateBroadcastAlertFragment extends BaseFragment {
    private AlertActivity activity;
    private CreateBroadcastAlertPresenter presenter;

    @Bind(R.id.bt_broadcast_alert_save)
    Button btSave;

    @Bind(R.id.et_broadcast_alert_title)
    EditText edTitle;

    @Bind(R.id.et_broadcast_alert_description)
    EditText edDescription;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        presenter = new CreateBroadcastAlertPresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_broadcast_alert, container, false);
        ButterKnife.bind(this, view);
        activity = (AlertActivity) this.getActivity();
        setSaveButtonOnClick();
        return view;
    }

    private void goBackListAlerts() {
        String initialFrag = "aleatorio";
        ListAlertsFragment listAlertsFragment = new ListAlertsFragment();
        FragmentManager.replaceFragment(R.id.container_alert, listAlertsFragment, initialFrag,
                true, getFragmentManager());
    }

    public void failure(){
        Toast.makeText(getContext(),
                "Falha ao enviar notificação.",
                Toast.LENGTH_LONG
        ).show();
        goBackListAlerts();
    }

    public void successfulAlert(){
        Toast.makeText(getContext(),
                "Uma notificação foi enviada!",
                Toast.LENGTH_LONG
        ).show();
        goBackListAlerts();
    }

    private void setSaveButtonOnClick(){
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = edTitle.getText().toString();
                String description = edDescription.getText().toString();
                presenter.sendBroadcastAlert(title, description);
            }
        });
    }
}
