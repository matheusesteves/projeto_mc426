package com.rep.organiza.organizarep.task.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.rep.organiza.organizarep.Constants;
import com.rep.organiza.organizarep.api.ResponseInterceptor;
import com.rep.organiza.organizarep.api.Services;
import com.rep.organiza.organizarep.base.BasePresenter;
import com.rep.organiza.organizarep.mock.UserAuthenticator;
import com.rep.organiza.organizarep.model.Task;
import com.rep.organiza.organizarep.model.Status;
import com.rep.organiza.organizarep.task.model.WeekDay;
import com.rep.organiza.organizarep.task.view.ListTasksFragment;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListTasksPresenter extends BasePresenter {
    private ListTasksFragment fragment;

    public ListTasksPresenter(ListTasksFragment fragment){
        this.fragment = fragment;
    }

    @NonNull
    private List<WeekDay> getWeekDaysMOCK() {
        List<WeekDay> days = new ArrayList<WeekDay>();

        WeekDay sun = new WeekDay(new Date(), Status.done);
        days.add(sun);
        WeekDay mon = new WeekDay(new Date(), Status.notTaskDay);
        days.add(mon);
        WeekDay tue = new WeekDay(new Date(), Status.exchanged);
        days.add(tue);
        WeekDay wed = new WeekDay(new Date(), Status.notTaskDay);
        days.add(wed);
        WeekDay thu = new WeekDay(new Date(), Status.late);
        days.add(thu);
        WeekDay fri = new WeekDay(new Date(), Status.toDo);
        days.add(fri);
        WeekDay sat = new WeekDay(new Date(), Status.notTaskDay);
        days.add(sat);
        return days;
    }

    public void loadTasks(){
        fragment.showLoading();
        ResponseInterceptor.getInstance().intercept(this.fragment.getContext(), this, getTasksCallback(),
                Services.GET_TASKS, new JsonObject());
    }

    @NonNull
    private Callback<JsonArray> getTasksCallback() {
        return new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                if (response.body() != null && response.isSuccessful()) {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Task>>() {}.getType();
                    List<Task> tasks = gson.fromJson(response.body(), listType);

                    showSuccessfulTasks(tasks);
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable throwable) {
                Log.d(Constants.REPOSITORY_TAG, "Falha ao recuperar tarefas");
                fragment.failure();
                return;
            }
        };
    }

    public void showSuccessfulTasks(List<Task> tasks){
        if(tasks == null || tasks.isEmpty()){
            fragment.showEmptyState();
        }else {
            UserAuthenticator.setAuthenticatedUserTask(tasks.get(0));

            for (Task t : tasks) {
                t.setWeekDays(getWeekDaysMOCK());
            }
            fragment.showTasks(tasks);
            fragment.hideLoading();
        }
    }
}
