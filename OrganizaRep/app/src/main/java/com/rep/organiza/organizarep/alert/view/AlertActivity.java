package com.rep.organiza.organizarep.alert.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.rep.organiza.organizarep.R;
import com.rep.organiza.organizarep.Util.FragmentManager;
import com.rep.organiza.organizarep.base.BaseActivity;
import com.rep.organiza.organizarep.task.view.CreateTaskFragment;

import butterknife.Bind;
import butterknife.ButterKnife;


public class AlertActivity  extends BaseActivity {
    @Bind(R.id.container_alert)
    FrameLayout containerFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);
        ButterKnife.bind(this);
        this.setNavbarOnClick(R.id.item_alerts);
        this.initFragment();
    }

    protected void initFragment() {
        String initialFrag = "aleatorio";
        ListAlertsFragment listAlertsFragment = new ListAlertsFragment();
        FragmentManager.replaceFragment(R.id.container_alert, listAlertsFragment, initialFrag,
                true, getSupportFragmentManager());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_action:
                FragmentManager.replaceFragment(R.id.container_alert, new CreateBroadcastAlertFragment(), "f2",
                        true, getSupportFragmentManager());
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void configureActionBar() {
        mActionBar = getSupportActionBar();
        if (mActionBar == null) {
            return;
        }

        mActionBar.setDisplayShowCustomEnabled(true);
    }
}
