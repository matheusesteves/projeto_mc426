package com.rep.organiza.organizarep.base;

import com.rep.organiza.organizarep.model.AuthenticatedUser;

// Padrão de projeto - Singleton

public class OrganizaRepApp {
    private static OrganizaRepApp instance;
    private AuthenticatedUser authenticatedUser;

    public static OrganizaRepApp getInstance(){
        if(instance == null) {
            instance = new OrganizaRepApp();
        }

        return instance;
    }

    public AuthenticatedUser getAuthenticatedUser() {
        return authenticatedUser;
    }

    public void setAuthenticatedUser(AuthenticatedUser authenticatedUser) {
        this.authenticatedUser = authenticatedUser;
    }

    public String getToken(){
        return authenticatedUser.getToken();
    }
}
