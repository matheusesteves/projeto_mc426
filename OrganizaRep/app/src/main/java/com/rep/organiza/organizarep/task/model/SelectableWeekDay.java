package com.rep.organiza.organizarep.task.model;

import com.rep.organiza.organizarep.model.Status;

import java.util.Date;

public class SelectableWeekDay extends WeekDay {
    private boolean isSelected;

    public SelectableWeekDay(Date name, boolean isSelected) {
        super(name, Status.notTaskDay);
        this.isSelected = isSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        if(selected){
            this.setStatus(Status.toDo);
        }else{
            this.setStatus(Status.notTaskDay);
        }

        isSelected = selected;
    }
}
