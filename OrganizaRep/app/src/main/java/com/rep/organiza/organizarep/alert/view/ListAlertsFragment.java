package com.rep.organiza.organizarep.alert.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.rep.organiza.organizarep.R;
import com.rep.organiza.organizarep.Util.FragmentManager;
import com.rep.organiza.organizarep.alert.model.BaseAlert;
import com.rep.organiza.organizarep.alert.model.DelayedTaskAlert;
import com.rep.organiza.organizarep.alert.presenter.ListAlertsPresenter;
import com.rep.organiza.organizarep.alert.view.adapters.AlertAdapter;
import com.rep.organiza.organizarep.base.BaseFragment;
import com.rep.organiza.organizarep.task.view.LoadingFragment;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListAlertsFragment extends BaseFragment {

    @Bind(R.id.rv_alerts)
    RecyclerView recyclerView;

    private AlertActivity activity;
    private ListAlertsPresenter presenter;
    private LoadingFragment loading;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        presenter = new ListAlertsPresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_alert, container, false);
        ButterKnife.bind(this, view);
        activity = (AlertActivity) this.getActivity();
        presenter.loadAlerts();

        return view;
    }

    public void showAlerts(List<BaseAlert> alerts) {
        if (alerts != null && !alerts.isEmpty()) {
            AlertAdapter adapter = new AlertAdapter(alerts, this.getContext(), this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);
        } else {
            showEmptyState();
        }
    }

    public void failure() {
        Toast.makeText(this.getContext(), "Ocorreu um erro ao tentar buscar os alertas", Toast.LENGTH_LONG).show();
    }


    public void showLoading(){
        loading = new LoadingFragment();
        FragmentManager.addFragment(R.id.container_alert, loading, "loading", false, getFragmentManager());
    }

    public void hideLoading(){
        FragmentManager.removeFragment(loading, getFragmentManager());
    }

    private void showEmptyState() {
        Toast.makeText(this.getContext(), "Não há alertas", Toast.LENGTH_LONG).show();
    }

    public RecyclerView getRecyclerView(){
        return this.recyclerView;
    }
}
