package com.rep.organiza.organizarep.alert.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rep.organiza.organizarep.R;
import com.rep.organiza.organizarep.alert.model.BaseAlert;
import com.rep.organiza.organizarep.alert.model.BroadcastAlert;
import com.rep.organiza.organizarep.alert.model.DelayedTaskAlert;
import com.rep.organiza.organizarep.alert.model.TypeAlert;
import com.rep.organiza.organizarep.alert.view.ListAlertsFragment;
import com.rep.organiza.organizarep.model.Task;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Delayed;

public class AlertAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<BaseAlert> listAlert;
    private Context mContext;
    private ListAlertsFragment mFragment;

    private static final int TYPE_DELAYED = 1;
    private static final int TYPE_CHANGE = 2;
    private static final int TYPE_UPDATE = 3;
    private static final int TYPE_BROADCAST = 4;

    public AlertAdapter(List<BaseAlert> list, Context context, ListAlertsFragment fragment) {
        this.listAlert = list;
        this.mContext = context;
        this.mFragment = fragment;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch(viewType){
            case TYPE_DELAYED:
                return new ViewHolderDelayed(
                        LayoutInflater.from(parent.getContext()).inflate(
                                R.layout.item_delayed_task, parent, false
                        )
                );
            case TYPE_CHANGE:
                return new ViewHolderDelayed(
                        LayoutInflater.from(parent.getContext()).inflate(
                                R.layout.item_delayed_task, parent, false
                        )
                );
            case TYPE_UPDATE:
                return new ViewHolderDelayed(
                        LayoutInflater.from(parent.getContext()).inflate(
                                R.layout.item_delayed_task, parent, false
                        )
                );
            case TYPE_BROADCAST:
                return new ViewHolderBroadcast(
                        LayoutInflater.from(parent.getContext()).inflate(
                                R.layout.item_broadcast_alert, parent, false
                        )
                );
            default:
                throw new RuntimeException("Não é um tipo de alerta possível");

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case TYPE_DELAYED:
                initLayoutDelayed((ViewHolderDelayed)holder, position);
                break;
            case TYPE_CHANGE:
                initLayoutChange((ViewHolderChange)holder, position);
                break;
            case TYPE_UPDATE:
                initLayoutUpdate((ViewHolderUpdate)holder, position);
                break;
            case TYPE_BROADCAST:
                initLayoutBroadcast((ViewHolderBroadcast)holder, position);
                break;
            default:
                break;
        }
    }

    private void initLayoutDelayed(ViewHolderDelayed holder, int pos) {
        BaseAlert alert = listAlert.get(pos);
        if (alert instanceof DelayedTaskAlert) {
            DelayedTaskAlert delayedTaskAlert = (DelayedTaskAlert)alert;
            Task task = delayedTaskAlert.getTask();
            if (delayedTaskAlert.getDate() != null && task != null && task.getTitle() != null) {
                holder.tvAlertTitle.setText(task.getTitle());
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = (Date) formatter.parse(delayedTaskAlert.getDate());
                    String stringDate = DateFormat.getDateInstance(DateFormat.MEDIUM).format(date);
                    holder.tvAlertDate.setText(stringDate);
                } catch (Exception e) {
                    holder.tvAlertDate.setText(delayedTaskAlert.getDate());
                }
            }
        }
    }

    private void initLayoutBroadcast(ViewHolderBroadcast holder, int pos){
        BaseAlert alert = listAlert.get(pos);
        if (alert instanceof BroadcastAlert){
            BroadcastAlert broadcastAlert = (BroadcastAlert)alert;
            String title = broadcastAlert.getTitle();
            String description = broadcastAlert.getDescription();
            if (title != null && description != null){
                holder.tvBroadcastTitle.setText(title);
                holder.tvBroadcastDescription.setText(description);
            }
        }
    }

    private void initLayoutChange(ViewHolderChange holder, int pos) {
        //holder.item.setText(listAlert.get(pos).getTitle());
    }

    private void initLayoutUpdate(ViewHolderUpdate holder, int pos) {
        //holder.item.setText(listAlert.get(pos).getTitle());
    }


    @Override
    public int getItemCount() {
        return listAlert != null ? listAlert.size() : 0;
    }

    @Override
    public int getItemViewType (int position) {
        BaseAlert alert = listAlert.get(position);
        if (alert.getType() == TypeAlert.late_task) {
            return TYPE_DELAYED;
        }
        if (alert.getType() == TypeAlert.change_task) {
            return TYPE_CHANGE;
        }
        if (alert.getType() == TypeAlert.update_task) {
            return TYPE_UPDATE;
        }
        if (alert.getType() == TypeAlert.broadcast_alert){
            return TYPE_BROADCAST;
        }
        return -1;
    }

    //TODO usar componentes do item
    static class ViewHolderDelayed extends RecyclerView.ViewHolder {
        TextView tvAlertTitle;
        TextView tvAlertDate;

        public ViewHolderDelayed(View itemView) {
            super(itemView);
            tvAlertTitle = itemView.findViewById(R.id.tv_title_delayed_task);
            tvAlertDate = itemView.findViewById(R.id.tv_date_delayed_task);
        }
    }

    static class ViewHolderChange extends RecyclerView.ViewHolder {
        public TextView item;
        public ViewHolderChange(View itemView) {
            super(itemView);
            item = (TextView) itemView.findViewById(R.id.tv_alert_task_title);
        }
    }

    static class ViewHolderUpdate extends RecyclerView.ViewHolder {
        public TextView item;
        public ViewHolderUpdate(View itemView) {
            super(itemView);
            item = (TextView) itemView.findViewById(R.id.tv_title_delayed_task);
        }
    }

    static class ViewHolderBroadcast extends RecyclerView.ViewHolder {
        TextView tvBroadcastTitle;
        TextView tvBroadcastDescription;

        public ViewHolderBroadcast(View itemView){
            super(itemView);
            this.tvBroadcastTitle = (TextView)itemView.findViewById(R.id.tv_title_broadcast_alert);
            this.tvBroadcastDescription = (TextView)itemView.findViewById(R.id.tv_description_broadcast_alert);
        }
    }
}
