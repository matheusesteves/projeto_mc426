package com.rep.organiza.organizarep.api;


import com.google.gson.JsonObject;
import com.rep.organiza.organizarep.base.BasePresenter;

import retrofit2.Callback;

class Request {

    private Services service;
    private Callback callback;
    private JsonObject args;
    private BasePresenter presenter;

    public Request(Services service, Callback callback, JsonObject args, BasePresenter presenter) {
        this.service = service;
        this.callback = callback;
        this.args = args;
        this.presenter = presenter;
    }

    public Services getService() {
        return service;
    }

    public void setService(Services service) {
        this.service = service;
    }

    public Callback getCallback() {
        return callback;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public JsonObject getArgs() {
        return args;
    }

    public void setArgs(JsonObject args) {
        this.args = args;
    }

    public BasePresenter getPresenter() {
        return presenter;
    }

    public void setPresenter(BasePresenter presenter) {
        this.presenter = presenter;
    }
}
