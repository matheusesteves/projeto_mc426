package com.rep.organiza.organizarep;

public class Constants {
    public static final int UNAUTHENTICATED = 403;

    public static final String REPOSITORY_TAG = "OrganizaAppRepoTag";
    public static final String ACTIVITY_CONTROL = "ACTIVITY_CONTROL";
    public static final String TEST = "testing";

    public static final String DATATRANSFERING_FROM_CREATETASK_TO_USERSELECT = "incompleteTask";
    public static final String DATATRANSFERING_FROM_TASKITEM_TO_CHANGETASK = "taskOtherUser";
    public static final String TOKEN = "token";
}
