package com.rep.organiza.organizarep.alert.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public enum TypeAlert implements Serializable {
    @SerializedName("late-task")
    late_task("late-task"),

    @SerializedName("change-task")
    change_task("change-task"),

    @SerializedName("update-task")
    update_task("update-task"),

    @SerializedName("broadcast-alert")
    broadcast_alert("broadcast-alert");

    private String key;

    TypeAlert(String key) { this.key = key; }

    public String getKey() {
        return key;
    }
}
