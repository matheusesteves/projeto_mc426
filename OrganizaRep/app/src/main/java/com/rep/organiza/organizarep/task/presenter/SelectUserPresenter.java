package com.rep.organiza.organizarep.task.presenter;

import android.support.annotation.NonNull;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.rep.organiza.organizarep.api.ResponseInterceptor;
import com.rep.organiza.organizarep.api.Services;
import java.lang.reflect.Type;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.rep.organiza.organizarep.base.BasePresenter;
import com.rep.organiza.organizarep.model.Task;
import com.rep.organiza.organizarep.model.User;
import com.rep.organiza.organizarep.task.model.SelectableUser;
import com.rep.organiza.organizarep.task.model.WeekDay;
import com.rep.organiza.organizarep.task.view.SelectUserFragment;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SelectUserPresenter extends BasePresenter {
    private SelectUserFragment fragment;
    private List<SelectableUser> listUsers;

    public SelectUserPresenter(SelectUserFragment fragment){
        this.fragment = fragment;
        listUsers = new ArrayList<SelectableUser>();
        loadUsers();
    }

    public void addTask(Task task){
        JsonObject json = buildTaskJson(task);
        ResponseInterceptor.getInstance().intercept(this.fragment.getContext(), this, saveTaskCallback(), Services.SAVE_TASK, json);
    }

    @NonNull
    private JsonObject buildTaskJson(Task task) {
        JsonObject memberJson = new JsonObject();
        memberJson.addProperty("name", task.getUser().getUserName());
        memberJson.addProperty("id", task.getUser().getId());
        memberJson.addProperty("photo_url", task.getUser().getUserImageUrl());

        JsonArray weekdaysJson = new JsonArray();

        for (WeekDay w : task.getWeekDays())
        {
            JsonObject weekdayJson = new JsonObject();

            Date d = w.getDate();
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            String dataStr = df.format(d);

            weekdayJson.addProperty("date", dataStr);
            weekdayJson.addProperty("status", w.getStatus().getKey());
            weekdaysJson.add(weekdayJson);
        }

        JsonObject taskJson = new JsonObject();

        taskJson.addProperty("title", task.getTitle());
        taskJson.addProperty("description", task.getDescription());
        taskJson.add("member", memberJson);

        taskJson.add("weekdays", weekdaysJson);

        JsonObject json = new JsonObject();
        json.add("task", taskJson);

        return json;
    }

    @NonNull
    private Callback saveTaskCallback() {
        return new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.body() != null && response.isSuccessful()) {
                    fragment.successfulTaskCreation();
                }
            }

            @Override
            public void onFailure(Call call, Throwable throwable) {
                fragment.failure();
            }
        };
    }

    public void loadUsers(){
        ResponseInterceptor.getInstance().intercept(this.fragment.getContext(), this, getUsersCallback(),
                Services.GET_USERS , new JsonObject());
    }

    @NonNull
    private Callback<JsonArray> getUsersCallback() {
        return new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                if (response.body() != null && response.isSuccessful()) {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<SelectableUser>>() {
                    }.getType();

                    listUsers = gson.fromJson(response.body(), listType);

                    fragment.showUsers(listUsers);
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable throwable) {
                fragment.failure();
            }
        };
    }


    public User getSelectedUser(){
        for(SelectableUser user: listUsers){
            if(user.isSelected()){
                return user;
            }
        }

        return null;
    }
}
