package com.rep.organiza.organizarep.api;

import android.content.Context;

import com.google.gson.JsonObject;
import com.rep.organiza.organizarep.base.BaseActivity;
import com.rep.organiza.organizarep.base.BasePresenter;
import com.rep.organiza.organizarep.helper.NetworkHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResponseInterceptor {

    private static ResponseInterceptor instance;


    public static ResponseInterceptor getInstance() {
        if (instance == null) {
            instance = new ResponseInterceptor();
        }

        return instance;
    }

    public static void setInstance(ResponseInterceptor instance) {
        ResponseInterceptor.instance = instance;
    }

    public <T> void intercept(final Context context, final BasePresenter presenter,
                              final Callback<T> callBack, final Services typeService,
                              final JsonObject args) {

        if (!NetworkHelper.isOnline(context)) {
            presenter.retryConnection(typeService);
            return;
        }

        Call<T> call = typeService.getCall(args);

        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(final Call<T> call, Response<T> response) {
                if (response.body() != null && response.isSuccessful()) {
                    callBack.onResponse(call, response);
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                callBack.onFailure(call, t);
            }
        });

    }

    private boolean isAlive(Context context) {
        BaseActivity activity = (BaseActivity) context;
        return activity != null && activity.isAlive();
    }

}
