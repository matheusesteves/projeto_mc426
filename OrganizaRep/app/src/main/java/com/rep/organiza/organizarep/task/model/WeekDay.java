package com.rep.organiza.organizarep.task.model;

import com.google.gson.annotations.SerializedName;
import com.rep.organiza.organizarep.model.Status;

import java.io.Serializable;
import java.util.Date;

public class WeekDay implements Serializable {
    @SerializedName("date")
    protected Date date;

    @SerializedName("status")
    private Status status;

    public WeekDay(Date date, Status status) {
        this.date = date;
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
