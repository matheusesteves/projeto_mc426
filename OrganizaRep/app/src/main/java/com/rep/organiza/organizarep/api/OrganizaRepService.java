package com.rep.organiza.organizarep.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface OrganizaRepService {

    @POST("auth/")
    Call<JsonObject> authorize(@Header("Authorization") String token);

    @GET("tasks/")
    Call<JsonArray> getTasks(@Header("Authorization") String token);

    @GET("members/")
    Call<JsonArray> getUsers(@Header("Authorization") String token);

    @POST("tasks/")
    Call<Object> saveTask(@Header("Authorization") String token, @Body JsonObject task);

    @GET("taskNotifications")
    Call<JsonArray> getDalayedTaskAlerts(@Header("Authorization") String token, @Query("per_page") int qtdPerPage, @Query("page") int page);

    @GET("notifications")
    Call<JsonArray> getAlerts(@Header("Authorization") String token, @Query("per_page") int qtdPerPage, @Query("page") int page);

    @POST("taskNotifications/")
    Call<Object> alertTask(@Header("Authorization") String token, @Body JsonObject task);

    @POST("broadcastNotifications/")
    Call<Object> sendBroadcastAlert(@Header("Authorization") String token, @Body JsonObject broadcastNotification);
}
