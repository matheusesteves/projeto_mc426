package com.rep.organiza.organizarep.model;

public class AuthenticatedUser extends User{
    private String token;
    private String name;
    private String image;

    public AuthenticatedUser(String token, String userName, String userImageUrl, String id) {
        super(userName, userImageUrl, id);
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
