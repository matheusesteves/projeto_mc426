package com.rep.organiza.organizarep.Util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {

    public static String dateToString(Date date){
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }

    public static String dateToString(Date date, String pattern){
        return new SimpleDateFormat(pattern).format(date);
    }
}
