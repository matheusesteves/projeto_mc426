package com.rep.organiza.organizarep.alert.model;

import com.google.gson.annotations.SerializedName;

public class BroadcastAlert extends BaseAlert{

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    public BroadcastAlert(String title, String description, TypeAlert type, String date){
        super(type, date);
        this.setTitle(title);
        this.setDescription(description);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
