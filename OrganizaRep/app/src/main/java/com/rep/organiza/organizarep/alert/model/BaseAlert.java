package com.rep.organiza.organizarep.alert.model;

import com.google.gson.annotations.SerializedName;

public class BaseAlert {

    @SerializedName("type")
    private TypeAlert type;

    @SerializedName("send_date")
    private String date;

    public BaseAlert(TypeAlert type, String date){
        this.setType(type);
        this.setDate(date);
    }

    public TypeAlert getType() {
        return type;
    }

    public void setType(TypeAlert type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
