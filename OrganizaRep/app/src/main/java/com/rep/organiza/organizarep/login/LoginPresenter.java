package com.rep.organiza.organizarep.login;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.google.gson.JsonObject;
import com.rep.organiza.organizarep.Constants;
import com.rep.organiza.organizarep.api.ResponseInterceptor;
import com.rep.organiza.organizarep.api.Services;
import com.rep.organiza.organizarep.base.BasePresenter;
import com.rep.organiza.organizarep.base.OrganizaRepApp;
import com.rep.organiza.organizarep.model.AuthenticatedUser;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter extends BasePresenter {
    private LoginActivity activity;
    private AuthenticatedUser authenticatedUser;

    public LoginPresenter(LoginActivity activity){
        this.activity = activity;
    }


    public FacebookCallback<LoginResult> setLogin() {
        return new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                setAuthentication(accessToken);
            }

            @Override
            public void onCancel() {
                activity.cancelLogin();
            }

            @Override
            public void onError(FacebookException e) {
                Log.e(Constants.REPOSITORY_TAG, e.toString());
                activity.failedLogin();
            }
        };
    }

    private void setAuthentication(AccessToken accessToken){
        JsonObject json = new JsonObject();
        json.addProperty(Constants.TOKEN, accessToken.getToken());

        ResponseInterceptor.getInstance().intercept(this.activity, this, authenticationCallback(accessToken),
                Services.AUTHENTICATE, json);
    }

    private Callback authenticationCallback(AccessToken accessToken) {
        return new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null && response.isSuccessful()) {
                    if (accessToken != null) {
                        getUserInfo(accessToken);
                        activity.successfulLogin();
                    }
                }else{
                    if(response.code() == Constants.UNAUTHENTICATED){
                        activity.unauthenticatedUser();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable throwable) {
                activity.failure();
            }
        };
    }

    private void setAuthenticatedUser(AuthenticatedUser authenticatedUser){
        OrganizaRepApp.getInstance().setAuthenticatedUser(authenticatedUser);
    }

    private void getUserInfo(AccessToken accessToken) {

        GraphRequest request = GraphRequest.newMeRequest(
                accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject data, GraphResponse response) {
                        try {
                            String token = accessToken.getToken();
                            String image = "";
                            if (data.has("picture")) {
                                image = data.getString("picture");
                            }

                            String id = data.getString("id");
                            String name = data.getString("name");

                            authenticatedUser = new AuthenticatedUser(token, name, image, id);
                            setAuthenticatedUser(authenticatedUser);
                            activity.successfulLogin();
                        }
                        catch (JSONException e){
                            Log.e(Constants.REPOSITORY_TAG, e.getMessage());
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public void setLoggedUser() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null && !accessToken.isExpired()) {
            if(authenticatedUser == null) {
                this.getUserInfo(accessToken);
            }else{
                setAuthenticatedUser(authenticatedUser);
            }
        }
    }
}
