# Esboço da arquitetura existente no projeto
Obs: No arquivo CONTRIBUTORS.md há uma lista de principais contribuintes em cada parte da arquitetura.
![Arquitetura_ProjetoES](/uploads/ba8603f7d3b94cd2276ad9e289fe3aab/Arquitetura_ProjetoES.jpg)

# Links úteis IHC
No semestre anterior, utilizamos a ideia desse projeto para as tarefas de prototipação da disciplina IHC, então temos alguns materiais.

[Apresentação sobre as ideias do projeto](https://drive.google.com/drive/folders/1SaKimFl81o78u6y9Ac32PhiPmfADd9F1)

[Protótipos das telas](https://www.figma.com/file/a1JjMt3BKdKRoNE9z5DtJmnO/Untitled?node-id=353%3A154)

# Troca de conhecimento entre membros
[Manual de uso para API Autenticada](https://docs.google.com/document/d/1A6e6F3MyMDWZvVf7S2UfG3niab5zkXglOq4oltPgf4M/edit)

[Instruções para ambiente e servidor django LOCAL](https://docs.google.com/document/d/1y_Ev8YWPqYVDg9MbZQGHxdN5RdB4EEvbwE2sWNhratE/edit)

# Levantamento de requisitos
[Roteiro para entrevista](https://docs.google.com/document/d/15bBRD0B6W0kJXnvNvo-DzfoSubwyG3_QPL77QCGn3u8/edit)

[Requisitos encontrados analisando as entrevistas](https://docs.google.com/document/d/1PoFCwDIZw8peOci9Ft6LSHb9R6sHLE9OIhEfYp3zWBk/edit?usp=sharing)

[Pasta com áudios de algumas entrevistas](https://drive.google.com/drive/folders/1bJZVZ7PqUXL66twXENliDH5yXrAGxBet?usp=sharing)

# GITFLOW

## ANDROID
1. Criar uma branch a partir da develop para desenvolver uma feature ou corrigir um bug.

Exemplos:
* rep_screen
* bug-fix-list-members
* feature/leave_rep

2. Uma vez que a feature está pronta/o bug está corrigido, a nova branch é mergeada na develop.

Merge branch 'feature/leave_rep' into develop(https://gitlab.com/matheusesteves/projeto_mc426/commit/56fe2020da1c74bf723b8cdb873b89154c4654ef)

Merge branch 'feature/broadcast-alert' into develop(https://gitlab.com/matheusesteves/projeto_mc426/commit/1127385868b45d1f3b9ce0c98ce158890b0a84af)

3. Depois que as features que cada um implementou estão todas juntas na develop, funcionando, a develop é mergeada na master e criamos uma tag de versão.

https://gitlab.com/matheusesteves/projeto_mc426/commit/0940daf949eb18eaaafd913a4f9cb791cb254271

Observação: antes, para a parte de android, tínhamos somente a branch master e as branches de feature. Então criavam-se branches a partir da master para a implementação das features e, conforme elas eram testadas, as branches de feature eram mergeadas na master.

## API
1. Criar uma branch a partir da develop para desenvolver uma feature ou corrigir um bug.

Exemplos:
* mark_task_as_finished
* feature/leave_rep
* hotfix/search_member

2. Uma vez que a feature está pronta/o bug está corrigido, a nova branch é mergeada na develop.

Merge branch 'feature/leave_rep' into 'develop'(https://gitlab.com/matheusesteves/projeto_mc426_django/commit/f199adc94aa4b9e51c6a6ef524f5d10375d0260d)

Merge branch 'mark_task_as_finished' into 'develop'(https://gitlab.com/matheusesteves/projeto_mc426_django/commit/0bf372b792e286373dbfa1c846f2f752201f9a6e)

3. Depois que as features que cada um implementou estão todas juntas na develop, funcionando, a develop é mergeada na master e criamos uma tag de versão.

Merge branch 'develop' into 'master'(https://gitlab.com/matheusesteves/projeto_mc426_django/commit/1721322c38d087f363a946db80ebbaaa99fa47af)

Obs: ao acrescentar endpoints e métodos na API, também atualizava-se um documento explicando o que cada novo método faz e como deve ser chamado pelo front-end.

Também fizemos pull requests com code review.(https://gitlab.com/matheusesteves/projeto_mc426_django/merge_requests/7)
