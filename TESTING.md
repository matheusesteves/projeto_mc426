Níveis dos testes que aplicaremos:
* unitário
* integração
* sistema.

Tipos de testes que aplicaremos:
* funcionalidade
* usabilidade

Técnicas de teste:
* estrutural para os unitários
* funcional e estrutural para integração e sistema

Sobre os testes unitários:
* serão automatizados no backend (Python), seguindo a arquitetura e os módulos já existentes criando estruturas de mock adequadas para cada testes.
* faremos uma rotina de testes automatizados através de Integração Contínua.

Sobre os testes de integração:
* testaremos os módulos do Projeto Android em conjunto com os módulos do Projeto Django, que contém nossa API. Criamos um ambiente de teste em cloud para ela (Heroku).

Sobre o teste de sistema:
* exploraremos o aplicativo manualmente, sem automação, procurando por problemas e verificando se os requisitos foram atendidos.

